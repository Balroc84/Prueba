<?php

namespace App\Http\Controllers;

use MeetupEvents;
use MeetupKeyAuthConnection;

class WebController extends Controller
{
    public function index() {
        $connection = new MeetupKeyAuthConnection(env('API_KEY_MEETUP'));
        $m = new MeetupEvents($connection);
        $events =  $m->getOpenEvents( array( 'zip'=>'10012','ordering' => 'distance' ));

        return view('list',compact('events'));
    }

    public function getEvent($id) {
        $connection = new MeetupKeyAuthConnection(env('API_KEY_MEETUP'));
        $m = new MeetupEvents($connection);
        $event =  $m->getEvent($id,array());

        return view('detail',compact('event'));
    }
}
